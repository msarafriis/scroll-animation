# Scroll Animation

Makes scrolling look nicer. Sounds really useful.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
